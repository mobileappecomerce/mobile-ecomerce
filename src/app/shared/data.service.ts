import { Injectable } from "@angular/core";

export interface IDataItem {
    id: number;
    name: string;
    description: string;
    imageSrc: Array<any>;
}

@Injectable({
    providedIn: "root"
})
export class DataService {

    private items = new Array<IDataItem>(
        {
            id: 1,
            name: "Item 1",
            description: "Description for Item 1",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 2,
            name: "Item 2",
            description: "Description for Item 2",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 3,
            name: "Item 3",
            description: "Description for Item 3",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 4,
            name: "Item 4",
            description: "Description for Item 4",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 5,
            name: "Item 5",
            description: "Description for Item 5",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 6,
            name: "Item 6",
            description: "Description for Item 6",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 7,
            name: "Item 7",
            description: "Description for Item 7",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 8,
            name: "Item 8",
            description: "Description for Item 8",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 9,
            name: "Item 9",
            description: "Description for Item 9",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 10,
            name: "Item 10",
            description: "Description for Item 10",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 11,
            name: "Item 11",
            description: "Description for Item 11",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 12,
            name: "Item 12",
            description: "Description for Item 12",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 13,
            name: "Item 13",
            description: "Description for Item 13",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 14,
            name: "Item 14",
            description: "Description for Item 14",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 15,
            name: "Item 15",
            description: "Description for Item 15",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        },
        {
            id: 16,
            name: "Item 16",
            description: "Description for Item 16",
            imageSrc: [
                "res://300x300.jpg",
                "~/../../examples/image/img/Default.png",
                "http://www.codeproject.com/KB/mobile/883465/NativeScript.png"
            ]
        }
    );

    getItems(): Array<IDataItem> {
        return this.items;
    }

    getItem(id: number): IDataItem {
        return this.items.filter((item) => item.id === id)[0];
    }
}
